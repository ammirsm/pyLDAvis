from rispy.config import TAG_KEY_MAPPING
import rispy
import csv
from langdetect import detect

def _merge_fileds_in_dict(_dict, list_of_fields, destination):
    target = ""
    for i in list_of_fields:
        try:
            if type(_dict[i]) == type([]):
                target = target + " " + ' '.join(_dict[i])
            else:
                target = target + " " + _dict[i]
        except:
            pass
    _dict[destination] = target


def ris_to_csv(ris_file):
    with open(ris_file, 'r') as file:
        entries = list(rispy.load(file))
    csv_file = "." + ris_file.split(".")[-2] + ".csv"

    fields = list(TAG_KEY_MAPPING.values())
    fields.append("whole_data")
    our_fields = ['type_of_reference', 'first_authors', 'secondary_authors', 'tertiary_authors', 'subsidiary_authors',
              'abstract', 'authors', 'place_published', 'alternate_title1', 'alternate_title2', 'alternate_title3',
              'keywords', 'short_title', 'secondary_title', 'tertiary_title', 'title']

    with open(csv_file, mode='w') as csv_file:
        writer = csv.DictWriter(csv_file, fieldnames=fields)
        writer.writeheader()
        for i in entries:
            _merge_fileds_in_dict(i, our_fields, "whole_data")
            if detect(i["title"]) == 'en':
                writer.writerow(i)
