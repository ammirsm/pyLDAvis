import math

import pyLDAvis.gensim_models as gensimvis
import pyLDAvis
import gensim

import pyd3.scale
from generate_scores import generate_scores_from_csv
import json
from itertools import combinations
import pyd3
import math

mdswidth = 530
mdsheight = 530

def circle_r(freq, circle_prop=0.25):
    return math.sqrt(((freq / 100) * mdswidth * mdsheight * circle_prop) / math.pi)

def area(x0, y0, r0, x1, y1, r1):
    d = math.sqrt((x0 - x1)**2 + (y0-y1)**2)
    R, r = max([r0, r1]), min([r1, r0])
    print(math.fabs(y1-y0), math.fabs(x1 - x0), r1 + r0)
    if not (math.fabs(x1 - x0) < r1 + r0 or math.fabs(y1 - y0) < r1 + r0):
        return 0
    rr0 = r0*r0
    rr1 = r1*r1
    d = math.sqrt((x1 - x0) * (x1 - x0) + (y1 - y0) * (y1 - y0))
    phi = (math.acos((rr0 + (d * d) - rr1) / (2 * r0 * d))) * 2
    theta = (math.acos((rr1 + (d * d) - rr0) / (2 * r1 * d))) * 2
    area1 = 0.5 * theta * rr1 - 0.5 * rr1 * math.sin(theta)
    area2 = 0.5 * phi * rr0 - 0.5 * rr0 * math.sin(phi)
    return area1 + area2


def area_second(x0, y0, r0, x1, y1, r1):
    d = math.sqrt((x0 - x1) ** 2 + (y0 - y1) ** 2)
    R, r = max([r0, r1]), min([r1, r0])
    if d > r + R or d < R - r:
        return 0
    part1 = r * r * math.acos((d * d + r * r - R * R) / (2 * d * r))
    part2 = R * R * math.acos((d * d + R * R - r * r) / (2 * d * R))
    part3 = 0.5 * math.sqrt((-d + r + R) * (d + r - R) * (d - r + R) * (d + r + R))
    return part1+part2+part3


def scale_generator(circles):
    xrange = [min(circles['x']), max(circles['x'])]
    xdiff = xrange[1] - xrange[0]
    xpad = 0.05
    yrange = [min(circles['y']), max(circles['y'])]
    ydiff = yrange[1] - yrange[0]
    ypad = 0.05
    if xdiff > ydiff:
        xScale = pyd3.scale.linear(
            range=[0, mdswidth],
            domain=[xrange[0] - xpad * xdiff, xrange[1] + xpad * xdiff]
        )
        yScale = pyd3.scale.linear(
            range=[mdsheight, 0],
            domain=[
                yrange[0] - 0.5 * (xdiff - ydiff) - ypad * xdiff,
                yrange[1] + 0.5 * (xdiff - ydiff) + ypad * xdiff,
            ]
        )
    else:
        xScale = pyd3.scale.linear(
            range=[0, mdswidth],
            domain=[
                xrange[0] - 0.5 * (ydiff - xdiff) - xpad * ydiff,
                xrange[1] + 0.5 * (ydiff - xdiff) + xpad * ydiff,
            ]
        )
        yScale = pyd3.scale.linear(
            range=[mdsheight, 0],
            domain=[yrange[0] - ypad * ydiff, yrange[1] + ypad * ydiff]
        )
    return yScale, xScale

BASE_DIR = './CulturalChanges/'

d = gensim.corpora.Dictionary.load(BASE_DIR + 'dictionary.dict')
c = gensim.corpora.MmCorpus(BASE_DIR + 'corpus.mm')
lda = gensim.models.LdaModel.load(BASE_DIR + 'topic.model')
lda_td = gensim.models.LdaModel.load(BASE_DIR + 'topic_td.model')

_list = ["lle", "mmds", "pcoa", "isomap"]
models = [(lda_td, "tf_idf", "TFIDF_scoring"), (lda, "bag_of_word", "BOW_scoring")]
results = {}

for model in models:
    with open(BASE_DIR + 'models/' + model[1] + ".json", "w") as f:
        the_dict = generate_scores_from_csv(BASE_DIR + model[2] + ".csv")
        json.dump(the_dict, f)
    for i in _list:
        vis_data = gensimvis.prepare(model[0], c, d, mds=i)
        circles = vis_data.topic_coordinates.to_dict(orient='list')
        numbers = [k for k in range(len(circles['topics']))]
        yScale, xScale = scale_generator(circles)
        all_area = 0
        for j in combinations(numbers, 2):
            the_area = area_second(
                float(xScale(circles['x'][j[0]])),
                float(yScale(circles['y'][j[0]])),
                float(circle_r(circles['Freq'][j[0]])),
                float(xScale(circles['x'][j[1]])),
                float(yScale(circles['y'][j[1]])),
                float(circle_r(circles['Freq'][j[1]]))
            )
            all_area += the_area
        all_circles = 0
        for j in range(len(circles['topics'])):
            all_circles += float(circle_r(circles['Freq'][j]))**2*math.pi
        printed = [i, model[1], (all_area/(530*530)), all_area/all_circles]
        print(printed)

        with open(BASE_DIR + "htmls/" + i + "-" + model[1] + ".html", "w") as f:
            pyLDAvis.save_html(
                vis_data,
                f,
                paper_data=json.dumps(the_dict),
                template_type="scientific_articles",
                ldavis_url="../../PyLDAvis/js/ldavis.v3.0.0.js"
            )

