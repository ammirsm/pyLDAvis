import gensim
from nltk.stem import WordNetLemmatizer
import numpy as np
import csv
import nltk
from collections import OrderedDict


def lemmatize_stemming(text):
    from nltk.stem.snowball import SnowballStemmer
    englishStemmer = SnowballStemmer("english")
    return englishStemmer.stem(WordNetLemmatizer().lemmatize(text, pos='v'))


def preprocess(text):
    result = []
    for token in gensim.utils.simple_preprocess(text):
        if token not in gensim.parsing.preprocessing.STOPWORDS and len(token) > 10:
            result.append(lemmatize_stemming(token))
    return result

def get_dict():
    d = gensim.corpora.Dictionary.load('dictionary.dict')
    c = gensim.corpora.MmCorpus('corpus.mm')
    lda = gensim.models.LdaModel.load('topic.model')
    lda_td = gensim.models.LdaModel.load('topic_td.model')

    a_csv_file = open("papers.csv", "r")
    dict_reader = csv.DictReader(a_csv_file)

    ordered_dict_from_csv = list(dict_reader)

    np.random.seed(2018)

    nltk.download('wordnet')

    a_csv_file = open("papers.csv", "r")
    dict_reader = csv.DictReader(a_csv_file)
    ordered_dict_from_csv = list(dict_reader)

    the_dict = OrderedDict()
    for sample_title in ordered_dict_from_csv:
        sample_title = sample_title["name"]
        # print(sample_title)
        bow_vector = d.doc2bow(preprocess(sample_title))
        related_topic = sorted(lda_td[bow_vector], key=lambda tup: -1 * tup[1])
        # print(related_topic[0])
        # print(the_dict)
        if related_topic[0][0] in the_dict.keys():
            the_dict[related_topic[0][0]][int(related_topic[0][1]*100)] = sample_title
        else:
            the_dict[related_topic[0][0]] = OrderedDict()
            the_dict[related_topic[0][0]][int(related_topic[0][1]*100)] = sample_title

    for j in the_dict:
        the_dict[j] = sorted(the_dict[j].items(), reverse=True)

    new_dict = {}
    for j in the_dict.keys():
        new_dict[j] = the_dict[j][:10]
    a_csv_file.close()

    return new_dict


def generate_scores_from_csv(csv_file_path):
    a_csv_file = open(csv_file_path, "r")
    dict_reader = csv.DictReader(a_csv_file)
    ordered_dict_from_csv = list(dict_reader)

    the_dict = {}
    for i in ordered_dict_from_csv:
        if i["cluster"] in the_dict.keys():
            the_dict[i["cluster"]].append(i)
        else:
            the_dict[i["cluster"]] = [i]

    for i in the_dict:
        newlist = sorted(the_dict[i], key=lambda k: k['score'], reverse=True)[:10]
        newnewlist = []
        for j in newlist:
            for b in range(2):
                newnewlist.append(j)
        the_dict[i] = newnewlist

    a_csv_file.close()
    return the_dict
